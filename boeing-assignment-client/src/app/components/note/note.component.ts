import {Component, OnInit} from '@angular/core';
import {Note} from '../../domain/note';
import {NoteService} from '../../service/note.service';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {

  note = new Note('', '');

  constructor(public noteService: NoteService) {}

  ngOnInit() {
  }

  addNote() {
    this.noteService.addNote(this.note.title, this.note.contents);
  }

}
