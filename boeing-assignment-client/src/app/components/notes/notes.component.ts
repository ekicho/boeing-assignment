import {Component, OnInit} from '@angular/core';
import {Note} from '../../domain/note';
import {NoteService} from '../../service/note.service';
import {NoteListener} from '../../service/note.service';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit, NoteListener {

  notes: Note[] = [
  ];

  constructor(public noteService: NoteService) {
    noteService.addNoteListener(this);
  }

  ngOnInit() {
    this.refreshNotes();
  }

  refreshNotes(): void {
    this.noteService.listNotes().subscribe(notes => this.notes = notes);
  }

  updateNote(): void {
    this.refreshNotes();
  }

}

