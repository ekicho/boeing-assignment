import {Injectable} from '@angular/core';
import {Note} from '../domain/note';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';

export interface NoteListener {
  updateNote(): void;
}

@Injectable({
  providedIn: 'root'
})

export class NoteService {

  private notesUrl = 'http://localhost:8080';
  private listNotesUrl = this.notesUrl + '/list';
  private addNotesUrl = this.notesUrl + '/add';
  private noteListener: NoteListener;

  constructor(private http: HttpClient) {}

  public listNotes(): Observable<Note[]> {
    return this.http.get<Note[]>(this.listNotesUrl);
  }

  addNote(noteTitle: string, noteContents: string): void {
    const note: Note = new Note(noteTitle, noteContents);

    const options = {headers: {'Content-Type': 'application/json'}};
    this.http.post<Note>(this.addNotesUrl, JSON.stringify(note), options).subscribe(
      myNote => this.noteListener.updateNote()
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }

  public addNoteListener(noteListener: NoteListener) {
    this.noteListener = noteListener;
  }
}
