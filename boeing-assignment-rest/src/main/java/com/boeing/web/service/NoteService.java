package com.boeing.web.service;

import java.util.List;

public interface NoteService {

	/**
	 * Logs the note string representation.
	 * 
	 * @param noteDomain the notes that will be logged
	 */
	void add(NoteDomain noteDomain);

	/**
	 * Lists the random notes.
	 * 
	 * @return the random notes
	 */
	List<NoteDomain> listNotes();

	/**
	 * Clears the storage.
	 * */
	void clear();

}
