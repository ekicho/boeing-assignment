package com.boeing.web.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service("noteService")
public class NoteServiceImpl implements NoteService {

	private static final List<NoteDomain> storage = new ArrayList<>();

	public NoteServiceImpl() {
		storage.add(new NoteDomain("Note title 1", "Note contents 1"));
		storage.add(new NoteDomain("Note title 2", "Note contents 2"));
	}

	@Override
	public void clear() {
		storage.clear();
	}
	
	@Override
	public void add(NoteDomain noteDomain) {
		storage.add(noteDomain);
	}

	@Override
	public List<NoteDomain> listNotes() {
		return storage;
	}

}
