package com.boeing.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.boeing.web.service.NoteDomain;
import com.boeing.web.service.NoteService;

@RestController
public class NoteController {

	@Autowired
	private NoteService noteService;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@CrossOrigin
	@ResponseBody
	public void addNote(@RequestBody NoteDomain noteDomain) {
		noteService.add(noteDomain);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@CrossOrigin
	public List<NoteDomain> listNote() {
		return noteService.listNotes();
	}

}
