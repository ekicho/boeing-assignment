package com.boeing.web.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class NoteServiceImplTest {

	private static final String TITLE = "TITLE";
	private static final String CONTENTS = "CONTENTS";
	
	private NoteServiceImpl target  = new NoteServiceImpl();
	
	@Before
	public void setUp() {
		target  = new NoteServiceImpl();
		target.clear();
	}
	
	@Test
	public void testAdd() {
		assertEquals(0, target.listNotes().size());
		
		target.add(new NoteDomain(TITLE, CONTENTS));
		
		assertEquals(1, target.listNotes().size());
	}
	
}
